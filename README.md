# King Pong Backend
## Install docker and its friends
```console
brew install docker && brew install docker-compose && brew install docker-machine && brew install docker-machine-nfs
```
## Install virtualbox
https://www.virtualbox.org/wiki/Downloads

## Create a new docker machine
```console
docker-machine create docker -d virtualbox --virtualbox-memory=2048
```

Set environment vars:
```console
eval "$(docker-machine env docker)"
```
Set up docker-machine-nfs
```console
docker-machine-nfs docker
```

## Configure host
After setting up docker, run `docker-machine ip docker` and copy that ip address

Then run:
```console
sudo nano /etc/hosts
```

Add this line there: 
```console
this.is.your.dockerip test.kingpong.dev
``` 

## Generate ssl key
Run these commands in order to configure ssl
```console
cd docker/nginx/ssl
```
```console
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout kingpong.dev.key \
    -new \
    -out kingpong.dev.crt \
    -subj /CN=\*.kingpong.dev \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /System/Library/OpenSSL/openssl.cnf \
        <(printf '[SAN]\nsubjectAltName=DNS:\*.kingpong.dev')) \
    -sha256 \
    -days 3650
```
```console
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain docker/nginx/ssl/kingpong.dev.crt
``` 

## Start docker containers
```console
docker-machine start docker
eval $(docker-machine env docker)
docker-compose up -d
```

## Run Migrations and load fixtures
```
make all
```
