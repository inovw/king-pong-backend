<?php

declare(strict_types=1);

namespace App\Controller\Game;

use App\Entity\Game;
use App\Entity\Player;
use App\Entity\Result;
use App\Entity\Team;
use App\Exception\EntityNotFoundException;
use App\Repository\PlayerRepository;
use App\Serializer\Model\Games;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class CreateGameController
{
    public function __construct(
        private PlayerRepository $playerRepository,
        private EntityManagerInterface $entityManager,
        private SerializerInterface $serializer
    ) {
    }

    #[Route(path: '/api/game/create')]
    public function __invoke(Request $request): JsonResponse
    {
        try {
            /** @var Games $gameModel */
            $gameModel = $this->serializer->deserialize($request->getContent(), Games::class, 'json');

            foreach ($gameModel->games as $game) {
                $gameEntity = new Game();
                foreach ($game->results as $result) {
                    $players = [];
                    foreach ($result->team->players as $playerModel) {
                        if (! $player = $this->playerRepository->findOneBy(['id' => $playerModel->id])) {
                            throw EntityNotFoundException::fromClassNameAndIdentifier(Player::class, $playerModel->id);
                        }
                        $players[] = $player;
                    }
                    $result = new Result(new Team($players), $result->score, $result->hasWon);

                    $gameEntity->addResult($result);
                }
                $gameEntity->complete();
                $this->entityManager->persist($gameEntity);
            }
            $this->entityManager->flush();

            return new JsonResponse();
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage(), $exception->getCode() > 0 ? $exception->getCode() : 500);
        }
    }
}
