<?php

declare(strict_types=1);

namespace App\Controller\Game;

use App\Repository\GameRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class GetAllGamesByDateController
{
    public function __construct(
        private GameRepository $gameRepository,
        private SerializerInterface $serializer
    )
    {
    }

    #[Route(path: "/api/games/{date}")]
    public function __invoke(\DateTimeImmutable $date): JsonResponse
    {
        $games           = $this->gameRepository->findByDate($date);
        $serializedGames = $this->serializer->serialize(
            $games,
            'json',
            ['groups' => ['game', 'team', 'player']]
        );

        return new JsonResponse($serializedGames, 200, [], true);
    }
}
