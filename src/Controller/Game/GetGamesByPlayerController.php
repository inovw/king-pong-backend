<?php

declare(strict_types=1);


namespace App\Controller\Game;

use App\Entity\Player;
use App\Repository\GameRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class GetGamesByPlayerController
{
    public function __construct(
        private GameRepository $gameRepository,
        private SerializerInterface $serializer)
    {}


    #[Route(path: "/api/player/{player}/games")]
    public function __invoke(Player $player): JsonResponse
    {
        $games           = $this->gameRepository->findByPlayer($player);
        $serializedGames = $this->serializer->serialize(
            $games,
            'json',
            ['groups' => ['game', 'team', 'player']]
        );

        return new JsonResponse($serializedGames, 200, [], true);
    }
}
