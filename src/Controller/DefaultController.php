<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController
{
    #[Route(path: '/api/test', name: 'test')]
    public function __invoke(): JsonResponse
    {
        return new JsonResponse([
            'Welcome to our great API!'
        ]);
    }
}
