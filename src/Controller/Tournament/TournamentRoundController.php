<?php

declare(strict_types=1);

namespace App\Controller\Tournament;

use App\Entity\Game;
use App\Entity\Team;
use App\Entity\Tournament;
use App\Exception\EntityNotFoundException;
use App\Knockout\RoundGenerator;
use App\Repository\GameRepository;
use App\Repository\TeamRepository;
use App\Repository\TournamentRepository;
use App\Serializer\Model\Tournament as TournamentModel;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class TournamentRoundController
{
    public function __construct(
        private RoundGenerator $roundGenerator,
        private TournamentRepository $tournamentRepository,
        private SerializerInterface $serializer,
        private TeamRepository $teamRepository,
        private EntityManagerInterface $entityManager,
        private GameRepository $gameRepository,
        private LoggerInterface $logger
    ) {
    }

    #[Route(path: "/api/tournament/round", methods: ["POST", "GET"])]
    public function __invoke(Request $request): JsonResponse
    {
        try {
            /** @var TournamentModel $tournamentModel */
            $tournamentModel = $this->serializer->deserialize($request->getContent(), TournamentModel::class, 'json');

            /** @var Tournament $tournament */
            if (! $tournament = $this->tournamentRepository->find($tournamentModel->tournamentId)) {
                throw EntityNotFoundException::fromClassNameAndIdentifier(Tournament::class, $tournamentModel->tournamentId);
            }
            if(!$winningTeam = $tournament->getWinningTeam()) {
                if(!$request->isMethod('GET')) {
                    $this->saveGames($tournamentModel);
                }
                $this->entityManager->flush();

                $games = $this->roundGenerator->generateRoundOfGames($tournament);
            } else {
                return new JsonResponse(
                    $this->serializer->serialize([
                        'tournamentId' => $tournament->getId(),
                        'winningTeam' => $winningTeam
                    ], 'json', ['groups' => ['game', 'team', 'player']]),
                    200, [],
                    true
                );
            }
            $serializedGames = $this->serializer->serialize($games, 'json', ['groups' => ['game', 'team', 'player']]);

            return new JsonResponse($serializedGames, 200, [], true);
        } catch (\Exception $exception) {
            $this->logger->critical($exception->getMessage());

            return new JsonResponse($exception->getMessage(), $exception->getCode() > 0 ? $exception->getCode() : 500);
        }
    }

    /**
     * @throws EntityNotFoundException
     */
    private function saveGames(TournamentModel $tournamentModel): void
    {
        if (! $tournamentModel->games) {
            return;
        }

        foreach ($tournamentModel->games as $gameModel) {
            /** @var Game $game */
            if (! $game = $this->gameRepository->find($gameModel->id)) {
                throw EntityNotFoundException::fromClassNameAndIdentifier(Game::class, $gameModel->id);
            }

            foreach ($gameModel->results as $result) {
                /** @var Team $team */
                if (! $team = $this->teamRepository->find($result->team->id)) {
                    throw EntityNotFoundException::fromClassNameAndIdentifier(Team::class, $result->team->id);
                }

                $game->setScoreForTeam($team, $result->score, $result->hasWon);
                $game->complete();
            }
        }
    }
}
