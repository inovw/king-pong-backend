<?php

declare(strict_types=1);

namespace App\Controller\Tournament;

use App\Entity\Tournament;
use App\Exception\EntityNotFoundException;
use App\Repository\TournamentRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class CheckForActiveTournamentController
{
    public function __construct(
        private TournamentRepository $tournamentRepository,
        private SerializerInterface $serializer
    )
    {
    }

    #[Route(path: "/api/tournament/check", methods: ["GET"])]
    public function __invoke(): JsonResponse
    {
        $tournaments = $this->tournamentRepository->findActiveTournaments();

        return new JsonResponse(
            $this->serializer->serialize($tournaments, 'json', ['groups' => ['active']]),
            200,
            [],
            true
        );


    }
}
