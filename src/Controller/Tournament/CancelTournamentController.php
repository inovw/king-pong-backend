<?php

declare(strict_types=1);

namespace App\Controller\Tournament;

use App\Entity\Tournament;
use App\Exception\EntityNotFoundException;
use App\Repository\TournamentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class CancelTournamentController
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    #[Route(path: "/api/tournament/{tournament}/cancel", methods: ["GET"])]
    public function __invoke(Tournament $tournament): JsonResponse
    {
        $tournament->cancelTournament();
        $this->entityManager->flush();

        return new JsonResponse('Tournament cancelled');
    }
}
