<?php

declare(strict_types=1);

namespace App\Controller\Tournament;

use App\Entity\Game;
use App\Entity\Team;
use App\Entity\Tournament;
use App\Knockout\TournamentSchemeGenerator;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class StartTournamentController
{
    public function __construct(
        private TournamentSchemeGenerator $tournamentSchemeGenerator
    )
    {}

    #[Route(path: "/api/tournament/start", methods: ["POST"])]
    public function __invoke(Request $request): JsonResponse
    {
        $playerIds = null;
        if($request->getContent()) {
            $playerIds = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        }
        try {
            $tournament = $this->tournamentSchemeGenerator->generate($playerIds);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($tournament->getId(), Response::HTTP_CREATED);
    }
}
