<?php

declare(strict_types=1);

namespace App\Controller\Player;

use App\Repository\PlayerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class GetPlayersController
{
    public function __construct(
        private PlayerRepository $playerRepository,
        private SerializerInterface $serializer
    )
    {}

    /**
     * @Route("/api/players", methods={"GET"})
     */
    #[Route(path: "/api/players")]
    public function __invoke(Request $request): JsonResponse
    {
        $players           = $this->playerRepository->findAll();
        $serializedPlayers = $this->serializer->serialize($players, 'json', ['groups' => 'player']);

        return new JsonResponse($serializedPlayers, 200, [], true);
    }
}
