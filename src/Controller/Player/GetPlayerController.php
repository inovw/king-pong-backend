<?php

declare(strict_types=1);

namespace App\Controller\Player;

use App\Entity\Player;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class GetPlayerController
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    #[Route(path: '/api/player/{player}')]
    public function __invoke(Player $player): JsonResponse
    {
        $serializedPlayers = $this->serializer->serialize($player, 'json', ['groups' => 'player']);

        return new JsonResponse($serializedPlayers, 200, [], true);
    }
}
