<?php

declare(strict_types=1);

namespace App\Controller\Player;

use App\Repository\PlayerRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

#[AsController]
class GetPlayersByDate
{
    public function __construct(
        private PlayerRepository $playerRepository,
        private SerializerInterface $serializer
    )
    {}

    #[Route(path: "/api/players/date/{date}")]
    public function __invoke(\DateTimeImmutable $date): JsonResponse
    {
        $players = $this->playerRepository->findPlayersWithGamesPlayedAndWonByDate($date);

        $serializedPlayers = $this->serializer->serialize($players, 'json', ['groups' => 'player']);

        return new JsonResponse($serializedPlayers, 200, [], true);
    }
}
