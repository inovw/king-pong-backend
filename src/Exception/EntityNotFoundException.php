<?php

declare(strict_types=1);

namespace App\Exception;

class EntityNotFoundException extends \Exception
{
    public static function fromClassNameAndIdentifier(string $className, int $id): self
    {
        return new self(\sprintf('No \'%s\' found with Id %d', $className, $id), 404);
    }
}
