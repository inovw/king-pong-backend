<?php

declare(strict_types=1);

namespace App\Serializer\Model;

class Games
{
    /**
     * @var Game[] $games
     */
    public array $games;

    public function getGame(): array
    {
        return $this->games;
    }

    public function addGame(Game $game): void
    {
        $this->games[] = $game;
    }

    public function removeGame(): void
    {

    }

}
