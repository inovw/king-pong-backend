<?php

declare(strict_types=1);

namespace App\Serializer\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

class Game
{
    public ?int $id = null;
    /**
     * @var Result[] $results
     */
    public array $results;

    public function getResult(): array
    {
        return $this->results;
    }

    public function addResult(Result $result): void
    {
        $this->results[] = $result;
    }

    public function removeResult(): void
    {

    }
}
