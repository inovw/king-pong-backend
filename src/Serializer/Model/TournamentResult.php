<?php

declare(strict_types=1);

namespace App\Serializer\Model;

class TournamentResult
{
    public TournamentTeam $team;
    public ?int $score = null;
    public bool $hasWon;
}
