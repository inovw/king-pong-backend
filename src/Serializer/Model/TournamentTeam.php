<?php

declare(strict_types=1);

namespace App\Serializer\Model;

class TournamentTeam
{
    public int $id;
    /**
     * @var Player[] $players
     */
    public array $players;

    public function getPlayer(): array
    {
        return $this->players;
    }

    public function addPlayer(Player $player): void
    {
        $this->players[] = $player;
    }

    public function removePlayer(): void
    {

    }
}
