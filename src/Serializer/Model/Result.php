<?php

declare(strict_types=1);

namespace App\Serializer\Model;

class Result
{
    public Team $team;
    public ?int $score = null;
    public bool $hasWon;
}
