<?php

declare(strict_types=1);

namespace App\Serializer\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Serializer\Model\Game;

class Tournament
{
    public int $tournamentId;
    /**
     * @var TournamentGame[] $games
     */
    public array $games = [];

    public function getGame(): array
    {
        return $this->games;
    }

    public function addGame(Game $game): void
    {
        $this->games[] = $game;
    }

    public function removeGame(): void
    {

    }
}
