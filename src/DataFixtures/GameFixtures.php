<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Game;
use App\Entity\Result;
use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class GameFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $games = \json_decode(\file_get_contents(
            __DIR__ . '/data/games.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        foreach ($games as $gameContent) {
            $game = new Game();
            foreach ($gameContent as $teamContent) {
                $playerIds = $teamContent['players'];
                $hasWon    = $teamContent['hasWon'];
                $players   = [];
                foreach ($playerIds as $playerId) {
                    $players[] = $this->getReference($playerId);
                }

                $team = new Team($players);
                $game->addResult(new Result($team, 15, $hasWon));
            }
            $game->complete();

            $manager->persist($game);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            PlayerFixtures::class,
        ];
    }
}
