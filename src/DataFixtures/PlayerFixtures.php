<?php

namespace App\DataFixtures;

use App\Entity\Player;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PlayerFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $players = json_decode(
            file_get_contents(__DIR__ . '/data/players.json'),
            true
        );

        foreach ($players as $playerConfiguration) {
            $player = new Player(
                $playerConfiguration['name'],
                $playerConfiguration['value'],
                $playerConfiguration['image']
            );

            $manager->persist($player);
            $this->addReference($playerConfiguration['id'], $player);
        }

        $manager->flush();
    }
}
