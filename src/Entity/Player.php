<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 *
 */
class Player
{
    /**
     * @Groups({"player"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @Groups({"player"})
     * @ORM\Column(type="string", length=50)
     */
    private string $name;

    /**
     * @Groups({"player"})
     * @ORM\Column(type="integer")
     */
    private int $skillValue;

    /**
     * @Groups({"player"})
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private string $imageUrl;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Team", mappedBy="players")
     */
    private Collection $teams;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tournament", mappedBy="winners")
     */
    private $tournamentsWon;

    public function __construct(string $name, int $skillValue, ?string $imageUrl)
    {
        $this->name       = $name;
        $this->skillValue = $skillValue;
        $this->imageUrl   = $imageUrl;
        $this->teams      = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSkillValue(): int
    {
        return $this->skillValue;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function getTeams(): array
    {
        return $this->teams->toArray();
    }
}
