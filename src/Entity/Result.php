<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 */
class Result
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @Groups({"game"})
     * @ORM\ManyToOne(targetEntity="Team", cascade={"persist", "remove"})
     */
    private Team $team;

    /**
     * @Groups({"game"})
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $score = null;

    /**
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="results")
     */
    private $game;

    /**
     * @Groups({"game"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $hasWon = null;

    public function __construct(Team $team, ?int $score = null, ?bool $hasWon = null)
    {
        $this->team   = $team;
        $this->score  = $score;
        $this->hasWon = $hasWon;
    }

    public function getTeam(): Team
    {
        return $this->team;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): void
    {
        $this->score = $score;
    }

    public function setGame($game): void
    {
        $this->game = $game;
    }

    public function fillKnockoutScore(int $score, bool $hasWon): void
    {
        $this->score  = $score;
        $this->hasWon = $hasWon;

        if (! $hasWon) {
            $this->team->removeFromTournament();
        }
    }

    public function getHasWon(): ?bool
    {
        return $this->hasWon;
    }

    public function setHasWon(bool $hasWon): void
    {
        $this->hasWon = $hasWon;
    }
}
