<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * @Groups({"team"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @MaxDepth(1)
     * @Groups({"team"})
     * @ORM\ManyToMany(targetEntity="App\Entity\Player", inversedBy="teams")
     */
    private Collection $players;

    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="competingTeams")
     */
    private ?Tournament $currentTournament = null;

    /**
     * @ORM\OneToMany(targetEntity="Result", mappedBy="team")
     */
    private $results;

    public function __construct(
        array $players,
        ?Tournament $tournament = null
    ) {
        Assert::minCount($players, 1, 'No players provided to create a team');
        Assert::allIsInstanceOf($players, Player::class);

        $this->players = new ArrayCollection($players);
        $this->currentTournament = $tournament;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function removeFromTournament(): void
    {
        $this->currentTournament = null;
    }
}
