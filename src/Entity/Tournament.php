<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TournamentRepository")
 */
class Tournament
{
    public const BARRAGE_ROUND  = 'Barrage';
    public const KNOCKOUT_ROUND = 'Knockout';
    public const COMPLETED = 'Completed';
    public const CANCELLED = 'Cancelled';

    public const ACTIVE_ROUNDS = [self::BARRAGE_ROUND, self::KNOCKOUT_ROUND];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"active"})
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $date;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"game"})
     */
    private string $activeRound;

    /**
     * @ORM\OneToMany(targetEntity="Game", mappedBy="tournament")
     */
    private Collection $games;

    /**
     * @ORM\OneToMany(targetEntity="Team", mappedBy="currentTournament")
     */
    private Collection $competingTeams;

    /**
     * @ORM\OneToOne(targetEntity="Team")
     */
    private ?Team $winningTeam = null;

    public function __construct()
    {
        $this->date        = new \DateTimeImmutable();
        $this->activeRound = self::BARRAGE_ROUND;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getActiveRound(): string
    {
        return $this->activeRound;
    }

    public function isInBarrageRound(): bool
    {
        return $this->activeRound === self::BARRAGE_ROUND;
    }

    public function proceedToRound(string $roundName): void
    {
        if (! \in_array($roundName, [self::BARRAGE_ROUND, self::KNOCKOUT_ROUND, self::COMPLETED])) {
            throw new \InvalidArgumentException('Invalid round passed to proceed to');
        }

        if ($roundName === $this->activeRound) {
            throw new \InvalidArgumentException('You are already on this round');
        }

        $this->activeRound = $roundName;
    }

    public function getCompetingTeams(): Collection
    {
        return $this->competingTeams;
    }

    public function completeTournament(Team $winningTeam): void
    {
        $this->winningTeam = $winningTeam;
        $this->proceedToRound(self::COMPLETED);
    }

    public function cancelTournament(): void
    {
        $this->activeRound = self::CANCELLED;
    }

    public function getWinningTeam(): ?Team
    {
        return $this->winningTeam;
    }
}
