<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameRepository")
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"game"})
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @Groups({"game"})
     * @ORM\Column(type="boolean")
     */
    private bool $isCompleted = false;

    /**
     * @Groups({"game"})
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeImmutable $playedAt = null;

    /**
     * @Groups({"game"})
     * @ORM\OneToMany(targetEntity="App\Entity\Result", mappedBy="game", cascade={"persist", "remove"})
     */
    private Collection $results;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tournament", inversedBy="games")
     */
    private ?Tournament $tournament = null;

    public function __construct(
        Tournament $tournament = null,
    )
    {
        $this->results = new ArrayCollection();
        $this->tournament = $tournament;
    }

    public static function create1V1Game(array $players, Tournament $tournament): self
    {
        $game = new self($tournament);

        foreach ($players as $player) {
            $team = new Team([$player], $tournament);
            $game->addResult(new Result($team));
        }

        return $game;
    }

    public static function createKnockoutGame(array $teams, Tournament $tournament): self
    {
        $game = new self($tournament);

        foreach ($teams as $team) {
            $game->addResult(new Result($team));
        }

        return $game;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isCompleted(): bool
    {
        return $this->isCompleted;
    }

    public function getPlayedAt(): ?\DateTimeImmutable
    {
        return $this->playedAt;
    }

    public function complete(): void
    {
        $this->isCompleted = true;
        $this->playedAt = new \DateTimeImmutable();

    }

    public function setScoreForTeam(Team $team, ?int $score, bool $hasWon): void
    {
        /** @var Result $result */
        foreach ($this->results as $result) {
            if ($result->getTeam() === $team) {
                if ($score) {
                    $result->setScore($score);
                }
                $result->setHasWon($hasWon);
                if (!$result->getHasWon()) {
                    $team->removeFromTournament();
                }
            }
        }
    }

    public function addResult(Result $result): void
    {
        $this->results->add($result);
        $result->setGame($this);
    }

    public function getResults(): Collection
    {
        return $this->results;
    }

    public function getTournament(): ?Tournament
    {
        return $this->tournament;
    }
}
