<?php

declare(strict_types=1);

namespace App\Knockout;

use App\Entity\Game;
use App\Entity\Team;
use App\Entity\Tournament;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;

class RoundGenerator
{
    public function __construct(
        private GameRepository $gameRepository,
        private EntityManagerInterface $entityManager
    ) {
    }

    public function generateRoundOfGames(Tournament $tournament): array
    {
        $games = $this->gameRepository->findBy([
            'isCompleted' => false,
            'tournament'  => $tournament,
        ]);

        if (\count($games)) {
            return $games;
        }

        if ($tournament->isInBarrageRound()) {
            $tournament->proceedToRound(Tournament::KNOCKOUT_ROUND);
        }

        $teams = $tournament->getCompetingTeams()->toArray();

        $gamesToReturn = [];
        $numberOfGames = \count($teams) / 2;
        if($numberOfGames < 1) {
            /** @var Team $winningTeam */
            $winningTeam = $tournament->getCompetingTeams()->first();
            $tournament->completeTournament($winningTeam);

            $this->entityManager->flush();
            return [];
        }

        for ($i = 0; $i < $numberOfGames; ++$i) {
            $randomTeams = [];
            for ($j = 0; $j < 2; ++$j) {
                \shuffle($teams);
                $randomTeams[] = \array_pop($teams);
            }

            $game            = Game::createKnockoutGame($randomTeams, $tournament);
            $gamesToReturn[] = $game;
            
            $this->entityManager->persist($game);
        }

        $this->entityManager->flush();

        return $gamesToReturn;
    }
}
