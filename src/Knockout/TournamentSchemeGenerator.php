<?php

declare(strict_types=1);

namespace App\Knockout;

use App\Entity\Game;
use App\Entity\Team;
use App\Entity\Tournament;
use App\Repository\PlayerRepository;
use Doctrine\ORM\EntityManagerInterface;

class TournamentSchemeGenerator
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private PlayerRepository $playerRepository
    ) {
    }

    public function generate(?array $playerIds): Tournament
    {
        if($playerIds) {
            $todaysPlayers = $this->playerRepository->findBy(['id' => $playerIds]);
            shuffle($todaysPlayers);
        } else {
            $todaysPlayers = array_map(static function(array $playerArray) {
                return $playerArray['player'];
            }, $this->playerRepository->findPlayersWithGamesPlayedAndWonByDate(new \DateTimeImmutable()));
        }

        $playerTotal              = \count($todaysPlayers);
        $numberOfBarragePlayers   = $this->getNumberOfBarragePlayers($playerTotal);
        $numberOfQualifiedPlayers = $playerTotal - $numberOfBarragePlayers;
        $qualifiedPlayers         = \array_slice($todaysPlayers, 0, $numberOfQualifiedPlayers);
        $barragePlayers           = \array_slice($todaysPlayers, $numberOfQualifiedPlayers - 1, $numberOfBarragePlayers);

        $tournament = new Tournament();
        $this->entityManager->persist($tournament);

        foreach ($qualifiedPlayers as $qualifiedPlayer) {
            $team = new Team([$qualifiedPlayer], $tournament);
            $this->entityManager->persist($team);
        }

        for ($i = 0; $i < $numberOfBarragePlayers / 2; ++$i) {
            $players = [];
            for ($j = 0; $j < 2; $j++){
                shuffle($barragePlayers);
                $players[] = array_pop($barragePlayers);
            }

            $game = Game::create1V1Game($players, $tournament);

            $this->entityManager->persist($game);
        }

        $this->entityManager->flush();

        return $tournament;
    }

    private function getNumberOfBarragePlayers(int $numberOfPlayers): int
    {
        return (int) ($numberOfPlayers - (2 ** \floor(\log($numberOfPlayers, 2)))) * 2;
    }
}
