<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    public function findByPlayer(Player $player): array
    {
        return $this->createQueryBuilder('game')
            ->select('game')
            ->join('game.results', 'result')
            ->join('result.team', 'team')
            ->join('team.players', 'player')
            ->where('player = :player')
            ->setParameter('player', $player)
            ->getQuery()
            ->getResult();
    }

    public function findByDate(\DateTimeImmutable $date): array
    {
        $endDate = $date->modify('+1 day');

        return $this->createQueryBuilder('game')
            ->select('game')
            ->where('game.playedAt BETWEEN :start AND :end')
            ->setParameter('start', $date )
            ->setParameter('end', $endDate)
            ->getQuery()
            ->getResult();
    }
}
