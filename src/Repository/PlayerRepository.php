<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Player;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

    public function findPlayersWithGamesPlayedAndWonByDate(\DateTimeImmutable $date): array
    {
        $startDate = new \DateTimeImmutable($date->format('Y-m-d'));
        $endDate = $startDate->modify('+1 day');

        $players = $this->createQueryBuilder('p')
            ->select('p as player', 'count(DISTINCT game) as gamesPlayed', 'SUM(CASE result.hasWon WHEN 1 THEN 1 ELSE 0 END) as gamesWon')
            ->join('p.teams', 'team')
            ->join('team.results', 'result')
            ->join('result.game', 'game')
            ->where('game.playedAt BETWEEN :start AND :end')
            ->setParameter('start', $startDate)
            ->setParameter('end', $endDate->format('Y-m-d') )
            ->orderBy('gamesWon', 'DESC')
            ->groupBy('p.id')
            ->getQuery()
            ->getResult();

        array_walk ( $players, static function (&$player) {
                $player['gamesWon'] = (int) $player['gamesWon'];
        });

        return $players;
    }

    public function findPlayersByDate(\DateTimeImmutable $date): array
    {
        $startDate = new \DateTimeImmutable($date->format('Y-m-d'));
        $endDate = $startDate->modify('+1 day');

        return $this->createQueryBuilder('p')
            ->select('p')
            ->join('p.teams', 'team')
            ->join('team.game', 'game')
            ->where('game.playedAt BETWEEN :start AND :end')
            ->setParameter('start', $startDate)
            ->setParameter('end', $endDate->format('Y-m-d') )
            ->groupBy('p.id')
            ->getQuery()
            ->getResult();
    }
}
