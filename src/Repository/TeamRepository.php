<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Team;
use App\Entity\Tournament;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Team::class);
    }

    public function getTeamsInTournament(Tournament $tournament):array
    {
        return $this->createQueryBuilder('team')
            ->join('team.game', 'game')
            ->innerjoin('game.tournament', 'tournament')
            ->andWhere('tournament = :tournament')
            ->andWhere('team.hasWon = true')
            ->setParameters(['tournament' => $tournament])
            ->getQuery()
            ->getResult();
    }

    public function findOneByIdAndGame()
    {
    }
}
