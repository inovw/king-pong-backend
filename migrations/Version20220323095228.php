<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220323095228 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE tournament_winner');
        $this->addSql('ALTER TABLE tournament ADD winning_team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tournament ADD CONSTRAINT FK_BD5FB8D9B9C05969 FOREIGN KEY (winning_team_id) REFERENCES team (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD5FB8D9B9C05969 ON tournament (winning_team_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tournament_winner (tournament_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_685C9E5E99E6F5DF (player_id), INDEX IDX_685C9E5E33D1A3E7 (tournament_id), PRIMARY KEY(tournament_id, player_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE tournament_winner ADD CONSTRAINT FK_685C9E5E33D1A3E7 FOREIGN KEY (tournament_id) REFERENCES tournament (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament_winner ADD CONSTRAINT FK_685C9E5E99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tournament DROP FOREIGN KEY FK_BD5FB8D9B9C05969');
        $this->addSql('DROP INDEX UNIQ_BD5FB8D9B9C05969 ON tournament');
        $this->addSql('ALTER TABLE tournament DROP winning_team_id');
    }
}
