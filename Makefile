ARGS = $(filter-out $@,$(MAKECMDGOALS))
PHP_EXEC = docker-compose exec php-fpm sh -c

.PHONY: seed
seed:
	$(PHP_EXEC) 'bin/console cache:clear'
	$(PHP_EXEC) 'bin/console doctrine:database:drop --force --if-exists'
	$(PHP_EXEC) 'bin/console doctrine:database:create'
	$(PHP_EXEC) 'bin/console do:mi:mi --no-interaction'
	$(PHP_EXEC) 'bin/console do:fi:lo --no-interaction'

.PHONY: console
console:
	$(PHP_EXEC) 'bin/console $(ARGS)'

.PHONY: install
install:
	docker-compose run php-fpm composer install

.PHONY: all
all: install seed
